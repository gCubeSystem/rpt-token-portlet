# Changelog

## [v1.3.0] = 2024-04-10
Client-exchange configuration for a dedicated client (#27204)

## [v1.2.0] - 2024-03-20

- Decode Button
- Updatet layout
- Added refresh token
- Added expiration
- Button for decode on https://jwt.io/

## [v1.0.1] - 2022-05-25

 - Bug #23411 fix to the context list when deployed at VRE level

## [v1.0.0] - 2021-05-14

 - First release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).