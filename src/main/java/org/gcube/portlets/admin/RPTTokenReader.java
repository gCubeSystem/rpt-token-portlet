package org.gcube.portlets.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.common.scope.impl.ScopeBean.Type;
import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectConfiguration;
//import org.gcube.oidc.rest.OpenIdConnectRESTHelper;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;
import org.gcube.portal.oidc.lr62.JWTCacheProxy;
import org.gcube.portal.oidc.lr62.LiferayOpenIdConnectConfiguration;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class RPTTokenReader
 */
public class RPTTokenReader extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log log = LogFactoryUtil.getLog(RPTTokenReader.class);

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		GroupManager gm = new LiferayGroupManager();
		try {
			User theUser = PortalUtil.getUser(renderRequest);
			String currentContext = getCurrentContext(renderRequest);
			ScopeBean bean = new ScopeBean(currentContext);
			List<String> userContexts = new ArrayList<String>();
			List<String> vreContexts = new ArrayList<String>();
			List<GCubeGroup> userGroups = gm.listGroupsByUser(theUser.getUserId());
			if (bean.is(Type.VRE)) {
				userContexts.add(currentContext);
				vreContexts.add(currentContext);
			} else {
				for (GCubeGroup g : userGroups) {
					// skipping these sites
					if (!(g.getFriendlyURL().equals("/guest") || g.getFriendlyURL().equals("/global"))) {
						if (g.getGroupName().equals(PortalContext.getConfiguration().getInfrastructureName())) {
							String context = gm.getInfrastructureScope(g.getGroupId());
							userContexts.add(context);
							if (context.split("/").length == 4) {
								vreContexts.add(context);
							}
						}
						if (g.getParentGroupId() > 0) {
							String context = gm.getInfrastructureScope(g.getGroupId());
							userContexts.add(context);
							if (context.split("/").length == 4) {
								vreContexts.add(context);
							}
						}
					}
				}
			}
			renderRequest.setAttribute("userGroups", userGroups);
			renderRequest.setAttribute("userContexts", userContexts);
			renderRequest.setAttribute("vreContexts", vreContexts);
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.render(renderRequest, renderResponse);
	}

	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		String context = ParamUtil.getString(resourceRequest, "context", null);
		System.out.println("Selected context=" + context);
		HttpServletRequest httpReq = PortalUtil
				.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));
		// JWTToken umaToken = null;
		JWTToken exchangedToken = null;

		GroupManager gm = new LiferayGroupManager();

		resourceResponse.setContentType("application/json");
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

		try {
			User theUser = PortalUtil.getUser(resourceRequest);
			OpenIdConnectConfiguration configuration = LiferayOpenIdConnectConfiguration.getConfiguration(httpReq);

			jsonObject.put("token_url", configuration.getTokenURL().toString());
			JWTCacheProxy jwtCacheProxy = JWTCacheProxy.getInstance();
			String sessionId = httpReq.getSession().getId();
			String urlEncodedContext = null;
			try {
				urlEncodedContext = URLEncoder.encode(context, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// Almost impossible
				log.error("Cannot URL encode context", e);
			}

			JWTToken authToken = jwtCacheProxy.getOIDCToken(theUser, sessionId);

			// umaToken = OpenIdConnectRESTHelper.queryUMAToken(configuration.getTokenURL(),
			// authToken.getAccessTokenAsBearer(), urlEncodedContext, null);

			Long companyId = PortalUtil.getCompanyId(httpReq);

			String exchangeClientId = PrefsPropsUtil.getString(companyId,
					"d4science.oidc-token-exchange-dedicated-client-id");
			String exchangeClientSecret = PrefsPropsUtil.getString(companyId,
					"d4science.oidc-token-exchange-dedicated-client-secret");

			exchangedToken = OpenIdConnectRESTHelperExtended.ExtendedQueryExchangeToken(
					configuration.getTokenURL(),
					authToken.getAccessTokenString(),
					urlEncodedContext,
					exchangeClientId,
					exchangeClientSecret,
					null);

			// log.debug("Got a new UMA token " + exchangedToken.getTokenEssentials());
		} catch (OpenIdConnectRESTHelperException e) {
			resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE, "" + e.getStatus());

			e.printStackTrace();
			jsonObject.put("success", false);
			jsonObject.put("comment", e.getMessage());
			resourceResponse.getWriter().println(jsonObject);
			super.serveResource(resourceRequest, resourceResponse);
			return;
		} catch (Exception e) {
			e.printStackTrace();
			jsonObject.put("success", false);
			jsonObject.put("comment", e.getMessage());
			resourceResponse.getWriter().println(jsonObject);
			super.serveResource(resourceRequest, resourceResponse);
			return;
		}

		jsonObject.put("success", true);
		jsonObject.put("access_token", exchangedToken.getAccessTokenString());
		jsonObject.put("refresh_token", exchangedToken.getRefreshTokenString());

		jsonObject.put("raw_token", exchangedToken.getRaw());

		jsonObject.put("access_token_exp", exchangedToken.getExp());
		jsonObject.put("essential", exchangedToken.getTokenEssentials());

		jsonObject.put("client_id", exchangedToken.getAzp());

		JSONArray audiences = JSONFactoryUtil.createJSONArray();
		List<String> list_audiences = exchangedToken.getAud();
		for (int i = 0; i < list_audiences.size(); i++) {
			audiences.put((String) list_audiences.get(i));
		}
		jsonObject.put("audience", audiences);

		resourceResponse.getWriter().println(jsonObject);
		super.serveResource(resourceRequest, resourceResponse);
	}

	private String getCurrentContext(RenderRequest request) {
		long groupId = -1;
		try {
			groupId = PortalUtil.getScopeGroupId(request);
			return getCurrentContext(groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getCurrentContext(long groupId) {
		try {
			PortalContext pContext = PortalContext.getConfiguration();
			return pContext.getCurrentScope("" + groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
