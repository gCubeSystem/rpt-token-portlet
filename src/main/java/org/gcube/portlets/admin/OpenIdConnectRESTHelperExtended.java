package org.gcube.portlets.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OpenIdConnectRESTHelperExtended extends OpenIdConnectRESTHelper {
    protected static final Logger logger = LoggerFactory.getLogger(OpenIdConnectRESTHelperExtended.class);

    /**
     * Queries from the OIDC server an exchanged token by using provided access
     * token, for the given audience (context),
     * in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param tokenUrl      the token endpoint {@link URL} of the OIDC server
     * @param authorization the auth token (the access token URLEncoded by the
     *                      "Bearer " string)
     * @param audience      the audience (context) where to request the issuing of
     *                      the ticket (URLEncoded)
     * @param permissions   a list of permissions, can be <code>null</code>
     * @return the issued token
     * @throws OpenIdConnectRESTHelperException if an error occurs (also an
     *                                          unauthorized call), inspect the
     *                                          exception for details
     */
    public static JWTToken ExtendedQueryExchangeToken(URL tokenUrl, String authorization, String audience, String client_id,
            String client_secret,
            List<String> permissions) throws OpenIdConnectRESTHelperException {

        // logger.info("Queried exchangeToken for context " + audience);

        // logger.info("token url " + tokenUrl);
        // logger.info("exchangedToken credentials " + client_id + " " + client_secret) ;



        Map<String, List<String>> params = new HashMap<>();
        Map<String, String> extraHeaders = new HashMap<>();

        params.put("subject_token", Arrays.asList(authorization));
        params.put("client_id", Arrays.asList(client_id));
        params.put("client_secret", Arrays.asList(client_secret));
        params.put("grant_type", Arrays.asList("urn:ietf:params:oauth:grant-type:token-exchange"));
        params.put("subject_token_type", Arrays.asList("urn:ietf:params:oauth:token-type:access_token"));
        params.put("requested_token_type", Arrays.asList("urn:ietf:params:oauth:token-type:access_token"));

        if (audience.startsWith("/")) {
            try {
                // logger.trace("Audience was provided in non URL encoded form, encoding it");
                audience = URLEncoder.encode(audience, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                logger.error("Cannot URL encode 'audience'", e);
            }
        }

        extraHeaders.put("X-D4Science-Context", audience);

        // try {
        // params.put("audience", Arrays.asList(URLEncoder.encode(audience, "UTF-8")));
        // } catch (UnsupportedEncodingException e) {
        // logger.error("Cannot URL encode 'audience'", e);
        // }
        if (permissions != null && !permissions.isEmpty()) {
            params.put(
                    "permission", permissions.stream().map(s -> {
                        try {
                            return URLEncoder.encode(s, "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            return "";
                        }
                    }).collect(Collectors.toList()));
        }

        // logger.info("Query exchangeToken, url " + tokenUrl);
        // logger.info("Query exchangeToken, params " + params);
        // logger.info("Query exchangeToken, extraHeaders " + extraHeaders);

        return ExtenedPerformQueryTokenWithPOST(tokenUrl, null, params, extraHeaders);
    }

    protected static JWTToken ExtenedPerformQueryTokenWithPOST(URL tokenURL, String authorization,
            Map<String, List<String>> params, Map<String, String> headers) throws OpenIdConnectRESTHelperException {

        // logger.debug("Querying access token from OIDC server with URL: {}", tokenURL);
        StringBuilder sb;
        try {
            HttpURLConnection httpURLConnection = ExtendedPerformURLEncodedPOSTSendData(tokenURL, params, authorization,
                    headers);

            sb = new StringBuilder();
            int httpResultCode = httpURLConnection.getResponseCode();
            logger.trace("HTTP Response code: {}", httpResultCode);
            String responseContentType = httpURLConnection.getContentType();
            if (responseContentType != null) {
                // logger.debug("Response content type is: {}", responseContentType);
            } else {
                responseContentType = "";
            }
            if (httpResultCode != HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(httpURLConnection.getErrorStream(), "UTF-8"));

                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                throw OpenIdConnectRESTHelperException.create("Unable to get token", httpResultCode,
                        responseContentType, sb.toString());
            } else {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));

                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            }
        } catch (IOException e) {
            throw new OpenIdConnectRESTHelperException("Unable to get the token", e);
        }
        return JWTToken.fromString(sb.toString());
    }

    protected static HttpURLConnection ExtendedPerformURLEncodedPOSTSendData(URL url, Map<String, List<String>> params,
            String authorization, Map<String, String> headers)
            throws IOException, ProtocolException, UnsupportedEncodingException {

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        con.setRequestProperty("Accept", "application/json");
        if (authorization != null) {
            // logger.debug("Adding authorization header as: {}", authorization);
            con.setRequestProperty("Authorization", authorization);
        }

        if (headers != null) {
            for (String key : headers.keySet()) {
                con.setRequestProperty(key, headers.get(key));
                // logger.info("setting header " + key + " : " + headers.get(key));
            }
        }
        OutputStream os = con.getOutputStream();

        String queryString = mapToQueryString(params);
        // logger.debug("Parameters query string is: {}", queryString);
        os.write(queryString.getBytes("UTF-8"));
        os.close();
        return con;
    }

}
